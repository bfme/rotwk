/* 
***********************************
The Revora Network Bar - Bar Core
***********************************

File: bar_system.js
Author: 
  Original: Blodo [Yuri_S] 
  Edited by: Dark Lord of the Sith (DLotS) and TheDeadPlayer
  
Description: This file is the core of the network bar and builds the previously declared sections, dropdowns and links

*/

// Declare global vars 
var GLOBAL_bar_id = 'rnb__body';
var GLOBAL_dropdowns_id = 'rnb__dropdowns';
var GLOBAL_dropdown_prefix = 'rnb__ddlevel';
var GLOBAL_bar_settings = new Array();
var GLOBAL_bar_ishovered = 0;

/* BEGIN EDIT */
var GLOBAL_section_prefix = 'rnb__section__';
var GLOBAL_sectioncontainer_prefix = 'rnb__sectioncontainer__';
/* END EDIT */

var GLOBAL_bar_elements = new Array();
var GLOBAL_bar_ids = new Array();
var GLOBAL_bar_tree = new Array( new Array() );

// Declare functions
function rnb_declare (type, vars) {
	
	int = GLOBAL_bar_elements.length;
	switch (type) {
		case 'section':
		
		if (vars.length < 6) {
			for (i = vars.length; i < 6; i++) {
				vars[i] = '';
			}
		}
		
		GLOBAL_bar_tree[0][GLOBAL_bar_tree[0].length] = int; // add the element to the root array
		GLOBAL_bar_tree[vars[0]] = new Array(); // create it's own tree, since a section is obviously going to have a dropdown
		GLOBAL_bar_ids[vars[0]] = int; // add the element name to the id repository
		GLOBAL_bar_elements[int] = new Array(); // populate the elements array
		GLOBAL_bar_elements[int]['_id'] = vars[0];
		GLOBAL_bar_elements[int]['_name'] = vars[1];
		GLOBAL_bar_elements[int]['_class'] = vars[2];
		GLOBAL_bar_elements[int]['_dclass'] = vars[3];
		GLOBAL_bar_elements[int]['_img'] = vars[4];
        /* BEGIN EDIT */
        GLOBAL_bar_elements[int]['_url'] = vars[5];
        /* END EDIT */
		
		break;
		case 'category':
		
		if (vars.length < 8) {
			for (i = vars.length; i < 8; i++) {
				vars[i] = '';
			}
		}
		
		GLOBAL_bar_tree[vars[0]][GLOBAL_bar_tree[vars[0]].length] = int; // add the element to the parent array
		GLOBAL_bar_tree[vars[1]] = new Array(); // this is a category, so we create a tree
		GLOBAL_bar_ids[vars[1]] = int; // add the element name to the id repository
		GLOBAL_bar_elements[int] = new Array(); // populate the elements array
		GLOBAL_bar_elements[int]['_type'] = 'category';
		GLOBAL_bar_elements[int]['_parent'] = vars[0];
		GLOBAL_bar_elements[int]['_id'] = vars[1];
		GLOBAL_bar_elements[int]['_name'] = vars[2];
		/* BEGIN EDIT */
		GLOBAL_bar_elements[int]['_addimgsrc'] = vars[3];
		/* END EDIT */
		GLOBAL_bar_elements[int]['_img'] = vars[4];
		GLOBAL_bar_elements[int]['_class'] = vars[5];
		GLOBAL_bar_elements[int]['_dclass'] = vars[6];
		/* BEGIN EDIT */
		GLOBAL_bar_elements[int]['_url'] = vars[7];
		/* END EDIT */
		
		/* BEGIN EDIT */
		if(GLOBAL_bar_settings["buttonPrefixCharCode"] != '' && vars[4] == '') // only add the prefix if it's not an image (or else it'll appear in the title attribute)
		{
			var num = GLOBAL_bar_settings["buttonPrefixCharCode"];
			GLOBAL_bar_elements[int]['_name'] = String.fromCharCode(num) +" "+ vars[2];
		}
		/* END EDIT */
		
		break;
		case 'link':
		
		if (vars.length < 6) {
			for (i = vars.length; i < 6; i++) {
				vars[i] = '';
			}
		}
		
		GLOBAL_bar_tree[vars[0]][GLOBAL_bar_tree[vars[0]].length] = int; // add the element to the parent array
		GLOBAL_bar_elements[int] = new Array(); // populate the elements array
		GLOBAL_bar_elements[int]['_type'] = 'link';
		GLOBAL_bar_elements[int]['_parent'] = vars[0];
		GLOBAL_bar_elements[int]['_name'] = vars[1];
		GLOBAL_bar_elements[int]['_url'] = vars[2];
		/* BEGIN EDIT */
		GLOBAL_bar_elements[int]['_addimgsrc'] = vars[3];
		/* END EDIT */
		GLOBAL_bar_elements[int]['_img'] = vars[4];
		GLOBAL_bar_elements[int]['_class'] = vars[5];
		
		/* BEGIN EDIT */
		if(GLOBAL_bar_settings["buttonPrefixCharCode"] != '' && vars[4] == '') // only add the prefix if it's not an image (or else it'll appear in the title attribute)
		{
			var num = GLOBAL_bar_settings["buttonPrefixCharCode"];
			GLOBAL_bar_elements[int]['_name'] = String.fromCharCode(num) +" "+ vars[1];
		}
		/* END EDIT */
		
		break;
	}
	
}

function rnb_construct () {
	
	div = document.createElement('div');
	div.id = GLOBAL_bar_id;
    
    /* BEGIN EDIT */
    div.style.position = GLOBAL_bar_settings["rnbbodyCSSPosition"];
    /* END EDIT */
    
	for (i = 0; i < GLOBAL_bar_tree[0].length; i++) {
		
		var id = GLOBAL_bar_tree[0][i];
		
		/* BEGIN EDIT */
		container = document.createElement('div');
		container.className = GLOBAL_bar_settings["defaultSectionContainerClass"];
		container.setAttribute('id', GLOBAL_sectioncontainer_prefix + GLOBAL_bar_elements[id]['_id']);
		/* END EDIT */
		
		section = document.createElement('a');
		section.onmouseover = new Function("rnb_buildDropdown('"+GLOBAL_bar_elements[id]['_id']+"', this, 1, 0); GLOBAL_bar_ishover = 1;");
		section.onmouseout = new Function("rnb_destroyDropdown(0);");

		
		
		if (GLOBAL_bar_elements[id]['_class'] != '') {
			section.className = GLOBAL_bar_elements[id]['_class'];
		} else {
			section.className = GLOBAL_bar_settings['defaultSectionClass'];
		}
		
		/* BEGIN EDIT */
		section.setAttribute('id', GLOBAL_section_prefix + GLOBAL_bar_elements[id]['_id']);
		/* END EDIT */
		
		if (GLOBAL_bar_elements[id]['_img']) {
		
			/* BEGIN EDIT */
			switch(GLOBAL_bar_elements[id]['_img'][1])
			{
				// image replaces the text ['_name']
				case 'replace' :
					linkchild = document.createElement('img');
					linkchild.setAttribute('src', GLOBAL_bar_elements[id]['_img'][0]);
					linkchild.setAttribute('alt', GLOBAL_bar_elements[id]['_name']);
					linkchild.setAttribute('title', GLOBAL_bar_elements[id]['_name']);
					
					section.appendChild(linkchild);
				break;
				
				// image precedes the text
				case 'precede' :
					linkchild__img = document.createElement('img');
					linkchild__img.setAttribute('src', GLOBAL_bar_elements[id]['_img'][0]);
					linkchild__img.setAttribute('alt', '');
					
					linkchild__text = document.createTextNode(GLOBAL_bar_elements[id]['_name']);
					
					section.appendChild(linkchild__img);
					section.appendChild(linkchild__text);
				break;
			}
			/* END EDIT */
			
		} else {
			linkchild = document.createTextNode(GLOBAL_bar_elements[id]['_name']);
			
			section.appendChild(linkchild);
		}
        
        /* BEGIN EDIT */
        if (GLOBAL_bar_elements[id]['_url']) {
            section.setAttribute('href', GLOBAL_bar_elements[id]['_url']);
        }
        /* END EDIT */
		
		/* BEGIN EDIT */
		container.appendChild(section)
		/* END EDIT */
		
		div.appendChild(container);
		
		/* BEGIN EDIT */
		//div.appendChild(section);
		/* END EDIT */
	}
	
	div2 = document.createElement('div');
	div2.id = GLOBAL_dropdowns_id;
	div2.onmouseover = new Function("GLOBAL_bar_ishover = 1;");
	div2.onmouseout = new Function("rnb_destroyDropdown(0);");
	
	document.body.insertBefore(div2, document.body.firstChild);
	document.body.insertBefore(div, document.body.firstChild);
	
}

function rnb_buildDropdown (parent_id, button_obj, section, level) {
	
	rnb_refreshHover(level);
	if (GLOBAL_bar_tree[parent_id].length < 1) return false;
	
	var GLOBAL_bar_ishover = 1;
	var parent_numid = GLOBAL_bar_ids[parent_id];
	
	div = document.createElement('div');
	div.id = GLOBAL_dropdown_prefix + level;
	if (GLOBAL_bar_elements[parent_numid]['_dclass']) {
		div.className = GLOBAL_bar_elements[parent_numid]['_dclass'];
	} else {
		div.className = GLOBAL_bar_settings['defaultDropdownClass'];
	}
	
	for (j = 0; j < GLOBAL_bar_tree[parent_id].length; j++) {
		
		var id = GLOBAL_bar_tree[parent_id][j];
		var link1 = document.createElement('a');
		
		if (GLOBAL_bar_elements[id]['_type'] == "category") {
			link1.onmouseover = new Function('rnb_buildDropdown("'+GLOBAL_bar_elements[id]['_id']+'", this, 0, '+(level+1)+'); window.status=""; return true;');
		} else {
			link1.onmouseover = new Function('rnb_refreshHover('+(level+1)+'); window.status=""; return true;');
			link1.onclick = new Function('return rnb_click(this);');
		}
		
		if (GLOBAL_bar_elements[id]['_url']) {
			link1.setAttribute('href', GLOBAL_bar_elements[id]['_url']);
		} else {
			link1.setAttribute('href', 'javascript:void(0);');
		}
		
		if (GLOBAL_bar_elements[id]['_class']) {
			link1.className = GLOBAL_bar_elements[id]['_class'];
		} else {
			link1.className = GLOBAL_bar_settings['defaultButtonClass'];
		}
		
		if (GLOBAL_bar_elements[id]['_img']) {
			linkchild = document.createElement('img');
			linkchild.setAttribute('src', GLOBAL_bar_elements[id]['_img']);
			linkchild.setAttribute('alt', GLOBAL_bar_elements[id]['_name']);
			linkchild.setAttribute('title', GLOBAL_bar_elements[id]['_name']);
		} else {
			linkchild = document.createTextNode(GLOBAL_bar_elements[id]['_name']);
		}
		
		link1.appendChild(linkchild);
		
		/* BEGIN EDIT */
		// add extra forum/released/new image(s)
			if(GLOBAL_bar_elements[id]['_addimgsrc'])
			{
				// parameter is an array -> add all elements as images
				if(typeof(GLOBAL_bar_elements[id]['_addimgsrc']) == "object")
				{
					// add an extra image for every element of the Array
					for(i=0; i<GLOBAL_bar_elements[id]['_addimgsrc'].length; i++)
					{
						addimg = document.createElement('img');
						addimg.setAttribute('src', GLOBAL_bar_elements[id]['_addimgsrc'][i]);
						addimg.setAttribute('alt', '');
						addimg.className = GLOBAL_bar_settings["defaultAddImageClass"];
						link1.appendChild(addimg);
					}
				}
				
				// parameter is a single string -> add the image
				else
				{
					addimg = document.createElement('img');
					addimg.setAttribute('src', GLOBAL_bar_elements[id]['_addimgsrc']);
					addimg.setAttribute('alt', '');
					addimg.className = GLOBAL_bar_settings["defaultAddImageClass"];
					link1.appendChild(addimg);
				}
			}
		/* END EDIT */
		
		div.appendChild(link1);
		
	}
	
	buttonobj2 = button_obj;
	var pos_left = 0;
	var pos_top = (level < 1) ? button_obj.offsetHeight : 0;
	if (isNaN(pos_top)) pos_top = 0;
	if (buttonobj2.offsetParent) {
		while (buttonobj2.offsetParent) {
			pos_left += buttonobj2.offsetLeft;
			pos_top += buttonobj2.offsetTop;
			buttonobj2 = buttonobj2.offsetParent;
		}
	} else if (buttonobj2.x) {
		pos_left += buttonobj2.x;
		pos_top += buttonobj2.y;
	}
	
	if (level > 0) pos_left += button_obj.offsetWidth;
	
    /* BEGIN EDIT */
//	div.style.position = 'absolute';
    div.style.position = GLOBAL_bar_settings["dropdownCSSPosition"];
    /* END EDIT */
	div.style.top = pos_top + 'px';
	div.style.left = pos_left + 'px';
	document.getElementById(GLOBAL_dropdowns_id).appendChild(div);

}

function rnb_refreshHover (level) {
	
	if (document.getElementById(GLOBAL_dropdown_prefix + level)) {
		for (j = level; document.getElementById(GLOBAL_dropdown_prefix + j); j++) {
			document.getElementById(GLOBAL_dropdowns_id).removeChild(document.getElementById(GLOBAL_dropdown_prefix + j));
		}
	}
	
}

function rnb_destroyDropdown (dele_v) {
	
	if (dele_v == 0) {
		GLOBAL_bar_ishover = 0;
		setTimeout("rnb_destroyDropdown(1);", 1000);
	} else {
		if (GLOBAL_bar_ishover == 0) {
			rnb_refreshHover(0);
		}
	}
	
}

function rnb_click (link) {

// tracker disabled by bart @ 2012 apr 15
	
//	title = link.text;
//	url = link.href;
	
//	uTitle = encodeURIComponent(title);
//	uUrl = encodeURIComponent(url);
//	location.href = rnb_path+"tracker.php?url="+uUrl+"&title="+uTitle;
		

//	return false;
	
}

// #EoF# //
